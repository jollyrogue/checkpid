package main

import (
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/go-yaml/yaml"
)

type ConfigTreeCheck struct {
	Name     string   `yaml:"name"`
	Path     string   `yaml:"path"`
	Interval uint16   `yaml:"interval"`
	Notify   []string `yaml:"notify"`
}

type ConfigTreeSocket struct {
	Server   string `yaml:"socket"`
	Type     string `yaml:"type"`
	Location string `yaml:"location"`
	Format   string `yaml:"format"`
	Secure   bool   `yaml:"secure"`
}

type ConfigTreeOutput struct {
	Socket ConfigTreeSocket `yaml:"socket"`
}

type ConfigTreeNotification struct {
	Socket   bool             `yaml:"socket"`
	StdOut   bool             `yaml:"stdout"`
	Output   ConfigTreeOutput `yaml:"output"`
	Server   string           `yaml:"server"`
	Type     string           `yaml:"type"`
	Location string           `yaml:"location"`
}

type ConfigTree struct {
	Checks        []ConfigTreeCheck      `yaml:"check"`
	Notifications ConfigTreeNotification `yaml:"notifications"`
	Quiet         bool                   `yaml:"debug"`
}

type AlertMessage struct {
	Service string
	Time    time.Time
	Notify  string
}

var quiet bool
var logLocation string

func init() {
	flag.BoolVar(&quiet, "q", false, "Disable output to stdout.")
	flag.StringVar(&logLocation, "l", "", "Set a log file if desired.")
}

func main() {
	// Channel to capture signals.
	signalFlare := make(chan os.Signal)
	// Channel to control the goroutines.
	work := make(chan bool)

	signal.Notify(signalFlare, syscall.SIGINT, syscall.SIGTERM)

	// Customizing the usage message.
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s /path/to/config.yaml\n", os.Args[0])
		flag.PrintDefaults()
		os.Exit(1)
	}

	flag.Parse()

	configFilePath := flag.Arg(0)

	if configFilePath == "" {
		fmt.Printf("Please specify a config file.\n\n")
		flag.Usage()
	}

	effectiveUID := os.Geteuid()
	if !quiet {
		log.Printf("Effective UID: %d\n", effectiveUID)
	}

	if effectiveUID != 0 {
		log.Printf("ERROR: root privileges are needed.\n")
		os.Exit(1)
	}

	// Setting up logging locations.
	if logLocation != "" {
		logfile, err := os.OpenFile(logLocation,
			os.O_RDWR|os.O_CREATE|os.O_APPEND,
			0644)
		if err != nil {
			log.Fatalf("Unable to open %s for logging, %v", logLocation, err)
			os.Exit(1)
		}
		defer logfile.Close()

		logOutMulti := io.MultiWriter(os.Stdout, logfile)
		log.SetOutput(logOutMulti)
		log.Printf("Stdout and %s set for logging.", logLocation)
	}

	configFileData, err := ioutil.ReadFile(configFilePath)
	if err != nil {
		log.Fatalln(err)
	}

	config := ConfigTree{}

	if err = yaml.Unmarshal(configFileData, &config); err != nil {
		log.Fatalln(err)
	}

	// Checking if quiet was set from the command line, which takes
	// precedence over config and env vars.
	if !quiet {

		// Setting debug from config file.
		switch config.Quiet {
		case true:
			quiet = true
		}

		// Setting debug from environmental variable, and taking precedence over
		// confg file.
		switch os.Getenv("CHECKPID_DEBUG") {
		case "True", "true":
			quiet = true
		}
	}

	if !quiet {
		log.Printf("Config file path: %s\n", configFilePath)
		log.Printf("Config file contents: %+v\n", config)
	}

	for _, check := range config.Checks {
		pid, err := pidFetch(check.Path)
		if err != nil || pid < 0 {
			log.Println("ERROR: ", err)
			continue
		}
		log.Printf("%s %s %d\n", check.Name, check.Path, pid)

		//TODO: Add a goroutine on a schedule.
		go processDead(pid, check)
	}

	sig := <-signalFlare
	log.Printf("Caught %s signal.", sig)
	close(work)

	log.Println("Stopping...")
}

func pidFetch(pidPath string) (pid int, err error) {
	//TODO: Return an error if things don't work out.
	pid = -1

	pidRaw, err := ioutil.ReadFile(pidPath)
	if err != nil {
		return pid, errors.New(fmt.Sprintf("Problem with %s. Error: %v",
			pidPath,
			err))
	} else if len(pidRaw) < 1 {
		return pid, errors.New(fmt.Sprintf("PID file %s is empty.", pidPath))
	}

	pidStr := strings.TrimSpace(string(pidRaw))
	pid, err = strconv.Atoi(pidStr)
	if err != nil {
		return pid, errors.New(fmt.Sprintf("Could not convert PID string to int. Error: %v", err))
	}

	return pid, nil
}

func workScheduler(work <-chan bool, pid int, cfgCheck ConfigTreeCheck) {
	clock := time.NewTicker(time.Duration(cfgCheck.Interval) * time.Second)
	go func() {
		for {
			select {
			case <-clock.C:
				processDead(pid, cfgCheck)
			case <-work:
				return
			}
		}
	}()
}

func processDead(pid int, cfgCheck ConfigTreeCheck) bool {
	process, err := os.FindProcess(pid)
	if err != nil {
		log.Printf("Unable to find the process %d", pid)
	}

	err = process.Signal(syscall.Signal(0))
	if err != nil {
		log.Println(err)
		log.Printf("Process %d is dead!", pid)
		return true
	} else {
		log.Println(err)
		log.Printf("Process %d is alive!", pid)
		return false
	}
}
